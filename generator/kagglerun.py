

import shutil
import os
from fastai.text import *

shutil.copy('../input/tmp_lm', '/kaggle/working/tmp_lm')

# os.mkdir('/kaggle/working/models')
shutil.copy('../input/model.pth', '/kaggle/working/model.pth')
shutil.copy('../input/itos.pkl', '/kaggle/working/itos.pkl')

# shutil.copy('../input/formatted_movie_lines.txt', '/kaggle/working/formatted_movie_lines.txt')


# data_lm = TextLMDataBunch.from_csv('/kaggle/working', 'formatted_movie_lines.txt',delimiter='\t')


# data_lm.save('tmp_lm')

data_lm = load_data('/kaggle/working', 'tmp_lm')   

# data_lm.show_batch()
# data_lm.save('data_lm_export.pkl')
# data_lm.vocab.itos[:20]

# data_clas.vocab.itos = data_lm.vocab.itos
#learn = language_model_learner(data_lm, arch=AWD_LSTM, pretrained=False, drop_mult=0.5,pretrained_fnames=["model", "itos"])
learn = language_model_learner(data_lm,arch=AWD_LSTM, drop_mult=0.5)
# learn.fit_one_cycle(1, 1e-2)

# learn.load_pretrained(wgts_fname='/kaggle/working/models/model.pth', itos_fname='/kaggle/working/models/itos.pkl')
learn.load_pretrained(wgts_fname='model.pth', itos_fname='itos.pkl',strict=False)
# learn.unfreeze()

# learn.fit_one_cycle(100, 1e-2)
# learn.load_encoder('fine_enc') 

res = learn.predict("missa sina asut", n_words=50)
print(res)
res = learn.predict("kauniit tissit", n_words=50)
print(res)
# learn.save_encoder('fine_enc')

# 
# learn.unfreeze()
# learn.fit_one_cycle(5, 1e-3)

# es = learn.predict("missa sina asut", n_words=50)
# print(res)
# res = learn.predict("kauniit tissit", n_words=50)
# print(res)

# res = learn.predict("missa sina asut", n_words=10)
# print(res)

