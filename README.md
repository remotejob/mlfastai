# mlfastai

kaggle datasets create -p data/
cd data; zip libs.zip libs/; cd ..

kaggle datasets version -p data/ -m "Updated data"


kaggle kernels push -p train/
kaggle kernels status sipvip/mlfastai
kaggle kernels output sipvip/mlfastai -p output/
cp output/tmp_lm data/
cp output/models/model.pth data/
cp output/formatted_movie_lines.txt data/
cp output/itos.pkl data/
kaggle datasets version -p data/ -m "Updated data 0"
kaggle datasets status sipvip/mlfastaidata


kaggle kernels push -p generator/
kaggle kernels status sipvip/mlfastaigen
kaggle kernels output sipvip/mlfastaigen -p output/
