
import shutil
# import os, sys
import torch
from fastai.text import *

shutil.copy('../input/formatted_movie_lines.txt', '/kaggle/working/formatted_movie_lines.txt')


data_lm = TextLMDataBunch.from_csv('/kaggle/working', 'formatted_movie_lines.txt',delimiter='\t')


data_lm.save('tmp_lm')

data_lm = load_data('/kaggle/working', 'tmp_lm')   

# data_lm.show_batch()
# data_lm.save('data_lm_export.pkl')
# data_lm.vocab.itos[:20]
learn = language_model_learner(data_lm, arch=AWD_LSTM, drop_mult=0.5)

learn.fit_one_cycle(1, 1e-2)

res = learn.predict("missa sina asut", n_words=50)
print(res)
res = learn.predict("kauniit tissit", n_words=50)
print(res)
# learn.save_encoder('model')
torch.save(learn.model.state_dict(), 'model.pth')
data_lm.vocab.save('itos.pkl')

learn.load_pretrained(wgts_fname='model.pth', itos_fname='itos.pkl')


# learn.load_encoder('model')
# res = learn.predict("missa sina asut", n_words=50)
# print(res)
# res = learn.predict("kauniit tissit", n_words=50)
# print(res)


# learn.unfreeze()
# learn.fit_one_cycle(2, 1e-3)

# res = learn.predict("missa sina asut", n_words=10)
# print(res)

